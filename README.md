# Nodejs samen eten server 🍕 Nodejs eating together server

Users can login and register to the server. They can add studenthomes and add meals to them.

For creating a studenthome or meal a user account is needed, but anyone can get a list of the available homes and meals. Updating or deleting a studenthome or a meal can only be done by the adminstrator user (the user who created the studenthome or meal).

## Endpoints

- '/'
- '/api/info'
- '/api/login'
- '/api/register'
- '/api/studenthome/'
- '/api/studenthome/:homeId'
- '/api/studenthome/:homeId'/meal
- '/api/studenthome/:homeId'/meal/:mealId
