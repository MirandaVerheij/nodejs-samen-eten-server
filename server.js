const express = require('express')
const app = express()
const logger = require('tracer').console()
const baseRoutes = require('./src/routes/baseRoutes')
const authRoutes = require('./src/routes/authenticationRoutes')
const homeRoutes = require('./src/routes/homeRoutes')
const mealRoutes = require('./src/routes/mealRoutes')
const setTZ = require('set-tz')
setTZ('UTC')
const port = process.env.PORT || 8080

app.use(express.json())

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,authorization')
  res.setHeader('Access-Control-Allow-Credentials', true)
  next()
})

app.all('*', (req, res, next) => {
  logger.info(req.method + ' request at ' + req.url)
  next()
})

app.use('/', baseRoutes)
app.use('/api', authRoutes)
app.use('/api/studenthome', mealRoutes)
app.use('/api/studenthome', homeRoutes)

app.all('*', (req, res, next) => {
  logger.info('Catch-all endpoint called!')
  next({ message: 'Endpoint ' + req.url + ' does not exist', errCode: 404 })
})

app.use((err, req, res, next) => {
  logger.error('Errorhandler called! ', err)
  res.status(err.errCode).json({
    error: 'Some error occurred',
    message: err.message
  })
  next()
})

app.listen(port, () => {
  logger.info(`Samen eten server listening at port: ${port}`)
})

module.exports = app
