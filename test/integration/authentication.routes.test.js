const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../server')
const pool = require('../../src/dao/database').pool
const logger = require('tracer').console()

chai.should()
chai.use(chaiHttp)

const CLEAR_DB = 'DELETE IGNORE FROM `user`'

describe('Authentication', () => {
  before(done => {
    pool.query(CLEAR_DB, (err, rows, fields) => {
      if (err) {
        logger.debug(`Error in the before: ${err}`)
        done(err)
      } else {
        done()
      }
    })
  })

  describe('UC101 Registation', () => {
    it('TC-101-1 should return a valid error when required value is missing', done => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          //First name is missing
          lastname: ' LastName ',
          email: 'test@test.nl',
          password: 'absecRet9?'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('firstname is missing or not a string')
          error.should.be.a('string')

          done()
        })
    })

    it('TC-101-2 should return a valid error when email address is invalid', done => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: ' LastName ',
          email: 'test.nl',
          password: 'secRet9?'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('email address is invalid')
          error.should.be.a('string')

          done()
        })
    })

    it('TC-101-3 should return a valid error when password is invalid', done => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: ' LastName ',
          email: 'test@test.nl',
          password: 'reallyBadPassword'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('password is invalid')
          error.should.be.a('string')

          done()
        })
    })

    it('TC-101-5 should return a token when providing valid information', done => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: 'LastName',
          email: 'test@test.nl',
          studentnr: 1234567,
          password: 'secreT99'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')

          const response = res.body
          response.should.have.property('token').which.is.a('string')
          done()
        })
    })

    it('TC-101-4 should return a valid error when email address is already taken by a user', done => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'SameEmail',
          lastname: 'AsSomeoneElse',
          email: 'test@test.nl',
          studentnr: 3455678,
          password: 'secreT99'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('This email address has already been taken.')

          done()
        })
    })
  })

  describe('UC102 Login', () => {
    it('TC-102-1 should return a valid error when required value is missing', done => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          //email address is missing
          password: 'secreT99'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message, errcode } = res.body
          message.should.be.a('string').that.equals('email address is missing or not a string')

          done()
        })
    })

    it('TC-102-2 should return a valid error when email address is invalid', done => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'different@test.nl',
          password: 'secreT99'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('User not found or password invalid')

          done()
        })
    })

    it('TC-102-3 should return a valid error when email address is invalid', done => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'test@test.nl',
          password: '123Secret'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('User not found or password invalid')

          done()
        })
    })

    it('TC-102-4 should return a valid error when user does not exist', done => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'hallo@test.nl',
          password: '123Secret'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('User not found or password invalid')

          done()
        })
    })

    it('TC-102-5 should return a token when providing valid information', done => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'test@test.nl',
          password: 'secreT99'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          const response = res.body
          response.should.have.property('token').which.is.a('string')
          done()
        })
    })
  })
})
