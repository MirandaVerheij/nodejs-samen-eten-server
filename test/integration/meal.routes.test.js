const chai = require('chai')
const expect = chai.expect
const chaiHttp = require('chai-http')
const server = require('../../server')
const database = require('../../src/dao/database')
const assert = require('assert')
const { request } = require('../../server')
const logger = require('tracer').console()
let token = ''
let tokenDifferentUser = ''

chai.should()
chai.use(chaiHttp)

describe('Manage Meal Routes', function () {
  describe('UC 301 - Create Meal', function () {
    before(done => {
      const sql =
        'DELETE FROM meal WHERE 1=1; DELETE FROM studenthome WHERE 1=1; DELETE FROM user WHERE 1=1; ALTER TABLE meal AUTO_INCREMENT = 1; ALTER TABLE user AUTO_INCREMENT = 1; ALTER TABLE studenthome AUTO_INCREMENT = 1;'
      database.pool.query(sql, (error, result) => {
        if (error) {
          logger.debug(error)
        } else {
          done()
        }
      })
    })

    it('TC-201-1 should create user and return a valid error when a required value is missing', function (done) {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'Tester',
          lastname: 'User',
          email: 'user@mail.nl',
          studentnr: 765432,
          password: 'geHEIMww55'
        })
        .end((err, res) => {
          chai
            .request(server)
            .post('/api/login')
            .send({
              email: 'user@mail.nl',
              password: 'geHEIMww55'
            })
            .end((err, res) => {
              res.body.should.have.property('token')
              token = res.body.token
              chai
                .request(server)
                .post('/api/studenthome/')
                .set({ Authorization: `Bearer ${token}` })
                .send({
                  Name: 'TestingHome1',
                  Address: 'Huizenstraat',
                  House_Nr: 12,
                  UserID: 1,
                  Postal_Code: '7898KL',
                  Telephone: '0123543230',
                  City: 'Dordrecht'
                })
                .end((err, res) => {
                  chai
                    .request(server)
                    .post('/api/studenthome/1/meal')
                    .set({ Authorization: `Bearer ${token}` })
                    .send({
                      //Name is missing
                      Description: 'Gezonde en lekkere salade',
                      Ingredients: 'Sla, tomaat, komkommer, olijfolie',
                      Allergies: 'Geen',
                      CreatedOn: '2021-05-19 20:00',
                      OfferedOn: '2021-05-25 18:00',
                      Price: 2.0,
                      MaxParticipants: 6,
                      UserID: 1,
                      StudenthomeID: 1
                    })
                    .end((err, res) => {
                      res.should.have.status(400)
                      res.should.be.an('object')

                      res.body.should.be.an('object').that.has.all.keys('message', 'error')

                      let { message, error } = res.body
                      message.should.be.a('string').that.equals('name is missing or not a string')
                      error.should.be.a('string')

                      done()
                    })
                })
            })
        })
    })

    it('TC-301-2 should return a valid error when user is not logged in', function (done) {
      chai
        .request(server)
        .post('/api/studenthome/1/meal')
        .send({
          Name: 'Salade',
          Description: 'Gezonde en lekkere salade',
          Ingredients: 'Sla, tomaat, komkommer, olijfolie',
          Allergies: 'Geen',
          CreatedOn: '2021-05-19 20:00',
          OfferedOn: '2021-05-25 18:00',
          Price: 2.0,
          MaxParticipants: 6,
          UserID: 1,
          StudenthomeID: 1
        })
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message } = res.body
          message.should.be.a('string').that.equals('authorization header missing!')

          done()
        })
    })

    it('TC-301-3 should successfully add meal', function (done) {
      chai
        .request(server)
        .post('/api/studenthome/1/meal')
        .set({ Authorization: `Bearer ${token}` })
        .send({
          Name: 'Salade',
          Description: 'Gezonde en lekkere salade',
          Ingredients: 'Sla, tomaat, komkommer, olijfolie',
          Allergies: 'Geen',
          CreatedOn: '2021-05-19 20:00',
          OfferedOn: '2021-05-25 18:00',
          Price: 2.0,
          MaxParticipants: 6,
          UserID: 1,
          StudenthomeID: 1
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('status', 'result')

          let { status, result } = res.body
          status.should.be.a('string').that.equals('success')
          expect(result).to.eql({
            Name: 'Salade',
            Description: 'Gezonde en lekkere salade',
            Ingredients: 'Sla, tomaat, komkommer, olijfolie',
            Allergies: 'Geen',
            CreatedOn: '2021-05-19 20:00',
            OfferedOn: '2021-05-25 18:00',
            Price: 2.0,
            MaxParticipants: 6,
            UserID: 1,
            StudenthomeID: 1
          })

          done()
        })
    })
  })

  describe('UC 302 - Update Meal', function () {
    it('TC-302-1 should return a valid error when a required value is missing', function (done) {
      chai
        .request(server)
        .put('/api/studenthome/1/meal/1')
        .set({ Authorization: `Bearer ${token}` })
        .send({
          //Allergies is missing
          Name: 'Salade',
          Description: 'Gezonde en lekkere salade',
          Ingredients: 'Sla, tomaat, komkommer, olijfolie',
          CreatedOn: '2021-05-19 20:00',
          OfferedOn: '2021-05-25 18:00',
          Price: 2.0,
          MaxParticipants: 6,
          UserID: 1,
          StudenthomeID: 1
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('allergies is missing or not a string')
          error.should.be.a('string')

          done()
        })
    })

    it('TC-302-2 should return a valid error when user is not logged in', function (done) {
      chai
        .request(server)
        .put('/api/studenthome/1/meal/1')
        .send({
          Name: 'Salade',
          Description: 'Gezonde en lekkere salade',
          Ingredients: 'Sla, tomaat, komkommer, olijfolie',
          Allergies: 'Geen',
          CreatedOn: '2021-05-19 20:00',
          OfferedOn: '2021-05-25 18:00',
          Price: 2.0,
          MaxParticipants: 6,
          UserID: 1,
          StudenthomeID: 1
        })
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message } = res.body
          message.should.be.a('string').that.equals('authorization header missing!')

          done()
        })
    })

    it('TC-302-3 return a valid error when user is not owner of the meal', function (done) {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'Another',
          lastname: 'Name',
          email: 'hello@test.com',
          studentnr: 918260,
          password: 'wAcHtWoOrD4'
        })
        .end((err, res) => {
          chai
            .request(server)
            .post('/api/login')
            .send({
              email: 'hello@test.com',
              password: 'wAcHtWoOrD4'
            })
            .end((err, res) => {
              res.body.should.have.property('token')
              tokenDifferentUser = res.body.token
              chai
                .request(server)
                .put('/api/studenthome/1/meal/1')
                .set({ Authorization: `Bearer ${tokenDifferentUser}` })
                .send({
                  Name: 'Salade',
                  Description: 'Gezonde en lekkere salade, nu met radijsjes',
                  Ingredients: 'Sla, tomaat, komkommer, radijs, olijfolie',
                  Allergies: 'Geen',
                  CreatedOn: '2021-05-19 20:00',
                  OfferedOn: '2021-05-25 18:00',
                  Price: 2.0,
                  MaxParticipants: 6,
                  UserID: 2,
                  StudenthomeID: 1
                })
                .end((err, res) => {
                  res.should.have.status(401)
                  res.should.be.an('object')

                  res.body.should.be.an('object').that.has.all.keys('message', 'error')

                  let { message } = res.body
                  message.should.be
                    .a('string')
                    .that.equals('user does not have permission to change this meal')

                  done()
                })
            })
        })
    })

    it('TC-302-4 should return a valid error when meal does not exist', function (done) {
      chai
        .request(server)
        .put('/api/studenthome/1/meal/5')
        .set({ Authorization: `Bearer ${tokenDifferentUser}` })
        .send({
          Name: 'Salade zonder tomaat',
          Description: 'Gezonde en lekkere salade',
          Ingredients: 'Sla, komkommer, olijfolie',
          Allergies: 'Geen',
          CreatedOn: '2021-05-19 20:00',
          OfferedOn: '2021-05-25 18:00',
          Price: 1.75,
          MaxParticipants: 4,
          UserID: 2,
          StudenthomeID: 1
        })
        .end((err, res) => {
          res.should.have.status(404)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message } = res.body
          message.should.be.a('string').that.equals('meal does not exist')

          done()
        })
    })

    it('TC-302-5 should successfully update meal', function (done) {
      chai
        .request(server)
        .put('/api/studenthome/1/meal/1')
        .set({ Authorization: `Bearer ${token}` })
        .send({
          Name: 'Salade zonder tomaat',
          Description: 'Gezonde en lekkere salade',
          Ingredients: 'Sla, komkommer, olijfolie',
          Allergies: 'Geen',
          CreatedOn: '2021-05-19 20:00',
          OfferedOn: '2021-05-25 18:00',
          Price: 1.75,
          MaxParticipants: 4,
          UserID: 1,
          StudenthomeID: 1
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('status', 'result')

          let { status, result } = res.body
          status.should.be.a('string').that.equals('success')
          expect(result).to.eql({
            Name: 'Salade zonder tomaat',
            Description: 'Gezonde en lekkere salade',
            Ingredients: 'Sla, komkommer, olijfolie',
            Allergies: 'Geen',
            CreatedOn: '2021-05-19 20:00',
            OfferedOn: '2021-05-25 18:00',
            Price: 1.75,
            MaxParticipants: 4,
            UserID: 1,
            StudenthomeID: 1
          })

          done()
        })
    })
  })

  describe('UC 303 - Get meals', function () {
    it('TC-303-1 should return a list of meals (one meal)', done => {
      chai
        .request(server)
        .get('/api/studenthome/1/meal')
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('status', 'result')

          let { status, result } = res.body
          status.should.be.a('string').that.equals('success')
          expect(result).to.eql([
            {
              ID: 1,
              Name: 'Salade zonder tomaat',
              Description: 'Gezonde en lekkere salade',
              Ingredients: 'Sla, komkommer, olijfolie',
              Allergies: 'Geen',
              CreatedOn: '2021-05-19T20:00:00.000Z',
              OfferedOn: '2021-05-25T18:00:00.000Z',
              Price: 1.75,
              MaxParticipants: 4,
              UserID: 1,
              StudenthomeID: 1
            }
          ])
          done()
        })
    })
  })

  describe('UC 304 - Get details of meal', function () {
    it('TC-304-1 should return a valid error when meal does not exist', done => {
      chai
        .request(server)
        .get('/api/studenthome/1/meal/5')
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          res.should.have.status(404)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message } = res.body
          message.should.be.a('string').that.equals('meal does not exist')

          done()
        })
    })

    it('TC-304-2 should return details of meal', done => {
      chai
        .request(server)
        .get('/api/studenthome/1/meal/1')
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('status', 'result')

          let { status, result } = res.body
          status.should.be.a('string').that.equals('success')
          expect(result).to.eql([
            {
              ID: 1,
              Name: 'Salade zonder tomaat',
              Description: 'Gezonde en lekkere salade',
              Ingredients: 'Sla, komkommer, olijfolie',
              Allergies: 'Geen',
              CreatedOn: '2021-05-19T20:00:00.000Z',
              OfferedOn: '2021-05-25T18:00:00.000Z',
              Price: 1.75,
              MaxParticipants: 4,
              UserID: 1,
              StudenthomeID: 1
            }
          ])
          done()
        })
    })
  })

  describe('UC 305 - Delete meal', function () {
    it('TC-305-2 should return a valid error when user is not logged in', function (done) {
      chai
        .request(server)
        .delete('/api/studenthome/1/meal/1')
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message } = res.body
          message.should.be.a('string').that.equals('authorization header missing!')

          done()
        })
    })

    it('TC-305-3 should return a valid error when user is not logged in', function (done) {
      chai
        .request(server)
        .delete('/api/studenthome/1/meal/1')
        .set({ Authorization: `Bearer ${tokenDifferentUser}` })
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message } = res.body
          message.should.be.a('string').that.equals('user does not have permission to change this meal')

          done()
        })
    })

    it('TC-305-4 should return a valid error when meal does not exist', done => {
      chai
        .request(server)
        .delete('/api/studenthome/1/meal/5')
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          res.should.have.status(404)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message } = res.body
          message.should.be.a('string').that.equals('meal does not exist')

          done()
        })
    })

    it('TC-305-5 should successfully delete meal', function (done) {
      chai
        .request(server)
        .delete('/api/studenthome/1/meal/1')
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('status')

          let { status } = res.body
          status.should.be.a('string').that.equals('success')

          done()
        })
    })
  })
})
