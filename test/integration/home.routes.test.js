const chai = require('chai')
const expect = chai.expect
const chaiHttp = require('chai-http')
const server = require('../../server')
const database = require('../../src/dao/database')
const assert = require('assert')
const logger = require('tracer').console()
let token = ''
let tokenDifferentUser = ''

chai.should()
chai.use(chaiHttp)

describe('Manage Studenthome Routes', function () {
  describe('UC 201 - Create Studenthome', function () {
    before(done => {
      const sql =
        'DELETE FROM studenthome WHERE 1=1; DELETE FROM user WHERE 1=1; ALTER TABLE studenthome AUTO_INCREMENT = 1; ALTER TABLE user AUTO_INCREMENT = 1'
      database.pool.query(sql, (error, result) => {
        if (error) {
          logger.debug(error)
        } else {
          done()
        }
      })
    })

    it('TC-201-1 should create user and return a valid error when a required value is missing', function (done) {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: 'LastName',
          email: 'test@test.nl',
          studentnr: 1234567,
          password: 'secreT99'
        })
        .end((err, res) => {
          chai
            .request(server)
            .post('/api/login')
            .send({
              email: 'test@test.nl',
              password: 'secreT99'
            })
            .end((err, res) => {
              res.body.should.have.property('token')
              token = res.body.token
              chai
                .request(server)
                .post('/api/studenthome/')
                .set({ Authorization: `Bearer ${token}` })
                .send({
                  //Name is missing
                  Address: 'Develstraat',
                  House_Nr: 5,
                  UserID: 1,
                  Postal_Code: '3131AB',
                  Telephone: '0987657777',
                  City: 'Barendrecht'
                })
                .end((err, res) => {
                  res.should.have.status(400)
                  res.should.be.an('object')

                  res.body.should.be.an('object').that.has.all.keys('message', 'error')

                  let { message, error } = res.body
                  message.should.be.a('string').that.equals('name is missing or not a string')
                  error.should.be.a('string')

                  done()
                })
            })
        })
    })

    it('TC-201-2 should return a valid error when postcode is invalid', function (done) {
      chai
        .request(server)
        .post('/api/studenthome/')
        .set({ Authorization: `Bearer ${token}` })
        .send({
          Name: 'Swimming',
          Address: 'Develstraat',
          House_Nr: 5,
          UserID: 1,
          Postal_Code: '0131AB',
          Telephone: '0987657777',
          City: 'Barendrecht'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('postal code is invalid')
          error.should.be.a('string')

          done()
        })
    })

    it('TC-201-3 should return a valid error when telephone number is invalid', function (done) {
      chai
        .request(server)
        .post('/api/studenthome/')
        .set({ Authorization: `Bearer ${token}` })
        .send({
          Name: 'Swimming',
          Address: 'Develstraat',
          House_Nr: 5,
          UserID: 1,
          Postal_Code: '3131AB',
          Telephone: '59876577772222',
          City: 'Barendrecht'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('telephone number is invalid')
          error.should.be.a('string')

          done()
        })
    })

    it('TC-201-4 should return a valid error when studenthome already exists', function (done) {
      chai
        .request(server)
        .post('/api/studenthome/')
        .set({ Authorization: `Bearer ${token}` })
        .send({
          Name: 'Swimming',
          Address: 'Develstraat',
          House_Nr: 5,
          UserID: 1,
          Postal_Code: '3131AB',
          Telephone: '0987657777',
          City: 'Barendrecht'
        })
        .end((err, res) => {
          res.should.have.status(200)
          chai
            .request(server)
            .post('/api/studenthome/')
            .set({ Authorization: `Bearer ${token}` })
            .send({
              Name: 'Swimming',
              Address: 'Develstraat',
              House_Nr: 5,
              UserID: 1,
              Postal_Code: '3131AB',
              Telephone: '0987657777',
              City: 'Barendrecht'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')

              res.body.should.be.an('object').that.has.all.keys('message')

              let { message } = res.body
              message.should.be.a('string').that.equals('studenthome already exists')

              done()
            })
        })
    })

    it('TC-201-5 should return a valid error when user is not logged in', function (done) {
      chai
        .request(server)
        .post('/api/studenthome/')
        .send({
          Name: 'Swimming',
          Address: 'Develstraat',
          House_Nr: 5,
          UserID: 1,
          Postal_Code: '3131AB',
          Telephone: '0987657777',
          City: 'Barendrecht'
        })
        .end((err, res) => {
          res.should.have.status(401)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message } = res.body
          message.should.be.a('string').that.equals('authorization header missing!')

          done()
        })
    })

    it('TC-201-4 should succesfully add studenthome', function (done) {
      chai
        .request(server)
        .post('/api/studenthome/')
        .set({ Authorization: `Bearer ${token}` })
        .send({
          Name: 'Studenthome2',
          Address: 'Develstraat',
          House_Nr: 6,
          UserID: 1,
          Postal_Code: '2222AB',
          Telephone: '0987650000',
          City: 'Breda'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('status', 'result')

          let { status, result } = res.body
          status.should.be.a('string').that.equals('success')
          expect(result).to.eql({
            Name: 'Studenthome2',
            Address: 'Develstraat',
            House_Nr: 6,
            UserID: 1,
            Postal_Code: '2222AB',
            Telephone: '0987650000',
            City: 'Breda'
          })

          done()
        })
    })
  })

  describe('UC 202 - Get Studenthomes', function () {
    before(done => {
      const sql =
        'DELETE FROM studenthome WHERE 1=1; DELETE FROM user WHERE 1=1; ALTER TABLE studenthome AUTO_INCREMENT = 1; ALTER TABLE user AUTO_INCREMENT = 1'
      database.pool.query(sql, (error, result) => {
        if (error) {
          logger.debug(error)
        } else {
          done()
        }
      })
    })

    it('TC-202-1 should return an empty list', done => {
      chai
        .request(server)
        .get('/api/studenthome/')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('status', 'result')

          let { status, result } = res.body
          status.should.be.a('string').that.equals('success')
          result.should.be.an('array').that.is.empty

          done()
        })
    })

    it('TC-202-2 should register user, create two studenthomes and return two studenthomes', done => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: 'LastName',
          email: 'test@test.nl',
          studentnr: 1234567,
          password: 'secreT99'
        })
        .end((err, res) => {
          chai
            .request(server)
            .post('/api/login')
            .send({
              email: 'test@test.nl',
              password: 'secreT99'
            })
            .end((err, res) => {
              res.body.should.have.property('token')
              token = res.body.token
              chai
                .request(server)
                .post('/api/studenthome/')
                .set({ Authorization: `Bearer ${token}` })
                .send({
                  Name: 'Swimming',
                  Address: 'Develstraat',
                  House_Nr: 5,
                  UserID: 1,
                  Postal_Code: '3131AB',
                  Telephone: '0987657777',
                  City: 'Barendrecht'
                })
                .end((err, res) => {
                  chai
                    .request(server)
                    .post('/api/studenthome/')
                    .set({ Authorization: `Bearer ${token}` })
                    .send({
                      Name: 'Studenthome2',
                      Address: 'Develstraat',
                      House_Nr: 6,
                      UserID: 1,
                      Postal_Code: '2222AB',
                      Telephone: '0987650000',
                      City: 'Breda'
                    })
                    .end((err, res) => {
                      chai
                        .request(server)
                        .get('/api/studenthome/')
                        .end((err, res) => {
                          assert.ifError(err)
                          res.should.have.status(200)
                          res.should.be.an('object')

                          res.body.should.be.an('object').that.has.all.keys('status', 'result')

                          let { status, result } = res.body
                          status.should.be.a('string').that.equals('success')
                          expect(result).to.eql([
                            {
                              ID: 1,
                              Name: 'Swimming',
                              Address: 'Develstraat',
                              House_Nr: 5,
                              UserID: 1,
                              Postal_Code: '3131AB',
                              Telephone: '0987657777',
                              City: 'Barendrecht'
                            },
                            {
                              ID: 2,
                              Name: 'Studenthome2',
                              Address: 'Develstraat',
                              House_Nr: 6,
                              UserID: 1,
                              Postal_Code: '2222AB',
                              Telephone: '0987650000',
                              City: 'Breda'
                            }
                          ])
                          done()
                        })
                    })
                })
            })
        })
    })

    it('TC-202-3 should return a valid error when search term is a non-existing city', done => {
      chai
        .request(server)
        .get('/api/studenthome?city=Atlantis')
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(404)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('city not found')
          error.should.be.a('string')

          done()
        })
    })

    it('TC-202-4 should return a valid error when search term is a non-existing name', done => {
      chai
        .request(server)
        .get('/api/studenthome?name=Zehltksniobza')
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(404)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('name not found')
          error.should.be.a('string')

          done()
        })
    })

    it('TC-202-5 should return a list of homes for the searched city', done => {
      chai
        .request(server)
        .get('/api/studenthome?city=Breda')
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(200)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('status', 'result')

          let { status, result } = res.body
          status.should.be.a('string').that.equals('success')
          expect(result).to.eql([
            {
              ID: 2,
              Name: 'Studenthome2',
              Address: 'Develstraat',
              House_Nr: 6,
              UserID: 1,
              Postal_Code: '2222AB',
              Telephone: '0987650000',
              City: 'Breda'
            }
          ])

          done()
        })
    })

    it('TC-202-6 should return a list of homes for the searched name', done => {
      chai
        .request(server)
        .get('/api/studenthome?name=Swimming')
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(200)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('status', 'result')

          let { status, result } = res.body
          status.should.be.a('string').that.equals('success')
          expect(result).to.eql([
            {
              ID: 1,
              Name: 'Swimming',
              Address: 'Develstraat',
              House_Nr: 5,
              UserID: 1,
              Postal_Code: '3131AB',
              Telephone: '0987657777',
              City: 'Barendrecht'
            }
          ])

          done()
        })
    })
  })

  describe('UC 203 - Get Studenthome Details', function () {
    before(done => {
      const sql =
        'DELETE FROM studenthome WHERE 1=1; DELETE FROM user WHERE 1=1; ALTER TABLE studenthome AUTO_INCREMENT = 1; ALTER TABLE user AUTO_INCREMENT = 1'
      database.pool.query(sql, (error, result) => {
        if (error) {
          logger.debug(error)
        } else {
          done()
        }
      })
    })

    it('TC-203-1 should return a valid error when search term is a non-existing studenthome id', done => {
      chai
        .request(server)
        .get('/api/studenthome/1')
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(404)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('studenthome does not exist')
          error.should.be.a('string')

          done()
        })
    })

    it('TC-203-2 should register user, add studenthome en return the home for the searched studenthome id', done => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: 'LastName',
          email: 'test@test.nl',
          studentnr: 1234567,
          password: 'secreT99'
        })
        .end((err, res) => {
          chai
            .request(server)
            .post('/api/login')
            .send({
              email: 'test@test.nl',
              password: 'secreT99'
            })
            .end((err, res) => {
              res.body.should.have.property('token')
              token = res.body.token
              chai
                .request(server)
                .post('/api/studenthome/')
                .set({ Authorization: `Bearer ${token}` })
                .send({
                  Name: 'Swimming',
                  Address: 'Develstraat',
                  House_Nr: 5,
                  UserID: 1,
                  Postal_Code: '3131AB',
                  Telephone: '0987657777',
                  City: 'Barendrecht'
                })
                .end((err, res) => {
                  chai
                    .request(server)
                    .get('/api/studenthome/1')
                    .end((err, res) => {
                      assert.ifError(err)
                      res.should.have.status(200)
                      res.should.be.an('object')

                      res.body.should.be.an('object').that.has.all.keys('status', 'result')

                      let { status, result } = res.body
                      status.should.be.a('string').that.equals('success')
                      expect(result).to.eql([
                        {
                          ID: 1,
                          Name: 'Swimming',
                          Address: 'Develstraat',
                          House_Nr: 5,
                          UserID: 1,
                          Postal_Code: '3131AB',
                          Telephone: '0987657777',
                          City: 'Barendrecht'
                        }
                      ])
                      done()
                    })
                })
            })
        })
    })
  })

  describe('UC 204 - Change Studenthome Details', function () {
    before(done => {
      const sql =
        'DELETE FROM studenthome WHERE 1=1; DELETE FROM user WHERE 1=1; ALTER TABLE studenthome AUTO_INCREMENT = 1; ALTER TABLE user AUTO_INCREMENT = 1'
      database.pool.query(sql, (error, result) => {
        if (error) {
          logger.debug(error)
        } else {
          done()
        }
      })
    })

    it('TC-204-1 should create a user and return a valid error when a required value is missing', done => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: 'LastName',
          email: 'test@test.nl',
          studentnr: 1234567,
          password: 'secreT99'
        })
        .end((err, res) => {
          chai
            .request(server)
            .post('/api/login')
            .send({
              email: 'test@test.nl',
              password: 'secreT99'
            })
            .end((err, res) => {
              res.body.should.have.property('token')
              token = res.body.token
              chai
                .request(server)
                .post('/api/studenthome/')
                .set({ Authorization: `Bearer ${token}` })
                .send({
                  Name: 'Swimming',
                  Address: 'Develstraat',
                  House_Nr: 5,
                  UserID: 1,
                  Postal_Code: '3131AB',
                  Telephone: '0987657777',
                  City: 'Barendrecht'
                })
                .end((err, res) => {
                  chai
                    .request(server)
                    .put('/api/studenthome/1')
                    .set({ Authorization: `Bearer ${token}` })
                    .send({
                      Name: 'Swimming',
                      //Address is missing
                      House_Nr: 10,
                      UserID: 1,
                      Postal_Code: '4422AB',
                      Telephone: '0987657777',
                      City: 'Barendrecht'
                    })
                    .end((err, res) => {
                      assert.ifError(err)
                      res.should.have.status(400)
                      res.should.be.an('object')

                      res.body.should.be.an('object').that.has.all.keys('message', 'error')

                      let { message, error } = res.body
                      message.should.be.a('string').that.equals('address is missing or not a string')

                      done()
                    })
                })
            })
        })
    })

    it('TC-204-2 should return a valid error when postal code is invalid', done => {
      chai
        .request(server)
        .put('/api/studenthome/1')
        .set({ Authorization: `Bearer ${token}` })
        .send({
          Name: 'Swimming',
          Address: 'Teststraat',
          House_Nr: 10,
          UserID: 1,
          Postal_Code: '123456',
          Telephone: '0987657777',
          City: 'Barendrecht'
        })
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('postal code is invalid')

          done()
        })
    })

    it('TC-204-3 should create a user and return a valid error when telephone number is invalid', done => {
      chai
        .request(server)
        .put('/api/studenthome/1')
        .set({ Authorization: `Bearer ${token}` })
        .send({
          Name: 'Swimming',
          Address: 'Teststraat',
          House_Nr: 10,
          UserID: 1,
          Postal_Code: '4422AB',
          Telephone: '1234567890',
          City: 'Barendrecht'
        })
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(400)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'error')

          let { message, error } = res.body
          message.should.be.a('string').that.equals('telephone number is invalid')

          done()
        })
    })

    it('TC-204-4 should return a valid error when studenthome does not exist', done => {
      chai
        .request(server)
        .put('/api/studenthome/0')
        .set({ Authorization: `Bearer ${token}` })
        .send({
          Name: 'Swimming',
          Address: 'Teststraat',
          House_Nr: 10,
          UserID: 1,
          Postal_Code: '4422AB',
          Telephone: '0987657777',
          City: 'Barendrecht'
        })
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(404)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message } = res.body
          message.should.be.a('string').that.equals('studenthome does not exist')

          done()
        })
    })

    it('TC-204-5 should return a valid error when user not logged in', done => {
      chai
        .request(server)
        .put('/api/studenthome/1')
        .send({
          Name: 'Swimming',
          Address: 'Teststraat',
          House_Nr: 10,
          UserID: 1,
          Postal_Code: '4422AB',
          Telephone: '0987657777',
          City: 'Barendrecht'
        })
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(401)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message } = res.body
          message.should.be.a('string').that.equals('authorization header missing!')

          done()
        })
    })

    it('TC-204-6 should successfully change studenthome', done => {
      chai
        .request(server)
        .put('/api/studenthome/1')
        .set({ Authorization: `Bearer ${token}` })
        .send({
          Name: 'Swimming',
          Address: 'Teststraat',
          House_Nr: 10,
          UserID: 1,
          Postal_Code: '4422AB',
          Telephone: '0987657777',
          City: 'Barendrecht'
        })
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(200)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('status', 'result')

          let { status, result } = res.body
          status.should.be.a('string').that.equals('success')
          expect(result).to.eql({
            Name: 'Swimming',
            Address: 'Teststraat',
            House_Nr: 10,
            UserID: 1,
            Postal_Code: '4422AB',
            Telephone: '0987657777',
            City: 'Barendrecht'
          })
          done()
        })
    })
  })

  describe('UC 205 - Delete Studenthome', function () {
    before(done => {
      const sql =
        'DELETE FROM studenthome WHERE 1=1; DELETE FROM user WHERE 1=1; ALTER TABLE studenthome AUTO_INCREMENT = 1; ALTER TABLE user AUTO_INCREMENT = 1'
      database.pool.query(sql, (error, result) => {
        if (error) {
          logger.debug(error)
        } else {
          done()
        }
      })
    })

    it('TC-205-1 should create a user and return a valid error when studenthome does not exist', done => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'FirstName',
          lastname: 'LastName',
          email: 'test@test.nl',
          studentnr: 1234567,
          password: 'secreT99'
        })
        .end((err, res) => {
          chai
            .request(server)
            .post('/api/login')
            .send({
              email: 'test@test.nl',
              password: 'secreT99'
            })
            .end((err, res) => {
              res.body.should.have.property('token')
              token = res.body.token
              chai
                .request(server)
                .delete('/api/studenthome/99')
                .set({ Authorization: `Bearer ${token}` })
                .end((err, res) => {
                  assert.ifError(err)
                  res.should.have.status(404)
                  res.should.be.an('object')

                  res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

                  let { message } = res.body
                  message.should.be.a('string').that.equals('studenthome does not exist')

                  done()
                })
            })
        })
    })

    it('TC-205-2 should return a valid error when user not logged in', done => {
      chai
        .request(server)
        .delete('/api/studenthome/1')
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(401)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('message', 'errcode')

          let { message } = res.body
          message.should.be.a('string').that.equals('authorization header missing!')

          done()
        })
    })

    it('TC-205-3 should create a user and return a valid error when user in not the owner', done => {
      chai
        .request(server)
        .post('/api/register')
        .send({
          firstname: 'Another',
          lastname: 'User',
          email: 'hello@test.nl',
          studentnr: 54321,
          password: 'hElloHello44'
        })
        .end((err, res) => {
          chai
            .request(server)
            .post('/api/login')
            .send({
              email: 'hello@test.nl',
              password: 'hElloHello44'
            })
            .end((err, res) => {
              tokenDifferentUser = res.body.token
              chai
                .request(server)
                .post('/api/studenthome/')
                .set({ Authorization: `Bearer ${token}` })
                .send({
                  Name: 'Swimming',
                  Address: 'Develstraat',
                  House_Nr: 5,
                  UserID: 1,
                  Postal_Code: '3131AB',
                  Telephone: '0987657777',
                  City: 'Barendrecht'
                })
                .end((err, res) => {
                  chai
                    .request(server)
                    .delete('/api/studenthome/1')
                    .set({ Authorization: `Bearer ${tokenDifferentUser}` })
                    .end((err, res) => {
                      assert.ifError(err)
                      res.should.have.status(401)
                      res.should.be.an('object')

                      res.body.should.be.an('object').that.has.all.keys('message', 'error')

                      let { message } = res.body
                      message.should.be
                        .a('string')
                        .that.equals('user does not have permission to change this studenthome')

                      done()
                    })
                })
            })
        })
    })

    it('TC-205-4 should successfully delete studenthome', done => {
      chai
        .request(server)
        .delete('/api/studenthome/1')
        .set({ Authorization: `Bearer ${token}` })
        .end((err, res) => {
          assert.ifError(err)
          res.should.have.status(200)
          res.should.be.an('object')

          res.body.should.be.an('object').that.has.all.keys('status')

          let { status } = res.body
          status.should.be.a('string').that.equals('success')

          done()
        })
    })
  })
})
