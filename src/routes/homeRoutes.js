const express = require('express')
const router = express.Router()
const homeController = require('../controllers/homeController')
const authController = require('../controllers/authController')

router.post(
  '/',
  authController.validateToken,
  homeController.validatePostalCode,
  homeController.validateTelephoneNumber,
  homeController.validateStudenthome,
  homeController.create
)
router.get('/', homeController.get)
router.get('/:homeId', homeController.getById)
router.put(
  '/:homeId',
  authController.validateToken,
  homeController.validatePostalCode,
  homeController.validateTelephoneNumber,
  homeController.validateStudenthome,
  homeController.validateStudenthomeNonExistent,
  homeController.update
)
router.delete(
  '/:homeId',
  authController.validateToken,
  homeController.validateStudenthomeNonExistent,
  homeController.delete
)

module.exports = router
