const express = require('express')
const router = express.Router()
const mealController = require('../controllers/mealController')
const authController = require('../controllers/authController')

router.post('/:homeId/meal', authController.validateToken, mealController.validateMeal, mealController.create)
router.get('/:homeId/meal', mealController.get)
router.get('/:homeId/meal/:mealId', mealController.getById)
router.put(
  '/:homeId/meal/:mealId',
  authController.validateToken,
  mealController.validateMeal,
  mealController.validateMealNonExistent,
  mealController.update
)
router.delete(
  '/:homeId/meal/:mealId',
  authController.validateToken,
  mealController.validateMealNonExistent,
  mealController.delete
)

module.exports = router
