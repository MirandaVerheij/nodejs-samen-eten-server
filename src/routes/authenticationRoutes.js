const express = require('express')
const router = express.Router()

const AuthController = require('../controllers/authController')

router.post('/login', AuthController.validateLogin, AuthController.login)
router.post(
  '/register',
  AuthController.validateRegister,
  AuthController.validateEmail,
  AuthController.validatePassword,
  AuthController.register
)

module.exports = router
