const express = require('express')
const logger = require('tracer').console()
const router = express.Router()

router.get('/', (req, res) => {
  logger.info('\nRoot endpoint called')
  res.status(200).json('Samen eten server in nodejs')
})

router.get('/api/info', (req, res) => {
  logger.info('\nInfo endpoint called')
  let result = {
    Studentnaam: 'Miranda Verheij',
    Studentnummer: '2157236',
    Beschrijving:
      'Deze server kan gebruikt worden door studenten om maaltijden aan te bieden in hun studentenhuis. Andere studenten kunnen zich dan aanmelden om deel te nemen aan deze maaltijden. ',
    'Sonarqube URL': ' ',
  }
  res.status(200).json(result)
})

module.exports = router
