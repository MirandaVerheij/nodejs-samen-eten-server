const mysql = require('mysql')
const logger = require('./config').logger
const dbconfig = require('./config').dbconfig

const pool = mysql.createPool(dbconfig)

pool.on('connection', function (connection) {
  logger.trace('Database connection established')
})

pool.on('acquire', function (connection) {
  logger.trace('Database connection aquired')
})

pool.on('release', function (connection) {
  logger.trace('Database connection released')
})

//When userID does not exist, the insert fails because of a FK_constraint
const add = (item, callback) => {
  const sql = 'INSERT INTO studenthome SET ?'
  pool.query(sql, item, (error, result) => {
    if (error) {
      callback(error.message, undefined)
    } else {
      callback(undefined, item)
    }
  })
}

const addMeal = (item, callback) => {
  const sql = 'INSERT INTO meal SET ?'
  pool.query(sql, item, (error, result) => {
    if (error) {
      callback(error.message, undefined)
    } else {
      callback(undefined, item)
    }
  })
}

const getAll = callback => {
  pool.query('SELECT * FROM studenthome', (error, result) => {
    if (error) {
      callback(error, undefined)
    } else {
      callback(undefined, result)
    }
  })
}

const getAllMeals = (id, callback) => {
  pool.query('SELECT * FROM meal WHERE StudenthomeID = ?', id, (error, result) => {
    if (error) {
      callback(error, undefined)
    } else {
      callback(undefined, result)
    }
  })
}

const getByCity = (city, callback) => {
  pool.query('SELECT * FROM studenthome WHERE City = ?', city, (error, result) => {
    if (error) {
      callback(error, undefined)
    } else if (result.length === 0) {
      callback({ message: 'city not found', errCode: 404 }, undefined)
    } else {
      callback(undefined, result)
    }
  })
}

const getByName = (name, callback) => {
  pool.query('SELECT * FROM studenthome WHERE Name = ?', name, (error, result) => {
    if (error) {
      callback(error, undefined)
    } else if (result.length === 0) {
      callback({ message: 'name not found', errCode: 404 }, undefined)
    } else {
      callback(undefined, result)
    }
  })
}

const getByCityAndName = (city, name, callback) => {
  pool.query('SELECT * FROM studenthome WHERE City = ? AND Name = ?', [city, name], (error, result) => {
    if (error) {
      callback(error, undefined)
    } else if (result.length === 0) {
      callback({ message: 'combination of name and city not found', errCode: 404 }, undefined)
    } else {
      callback(undefined, result)
    }
  })
}

const getById = (id, callback) => {
  pool.query('SELECT * FROM studenthome WHERE ID = ?', id, (error, result) => {
    if (error) {
      callback(error, undefined)
    } else if (result.length === 0) {
      callback({ message: 'studenthome does not exist', errCode: 404 }, undefined)
    } else {
      callback(undefined, result)
    }
  })
}

const getByIdMeal = (id, callback) => {
  pool.query('SELECT * FROM meal WHERE ID = ?', id, (error, result) => {
    if (error) {
      callback(error, undefined)
    } else if (result.length === 0) {
      callback({ message: 'meal does not exist', errCode: 404 }, undefined)
    } else {
      callback(undefined, result)
    }
  })
}

const update = (homeID, item, callback) => {
  const sql =
    'UPDATE studenthome SET Name = ?, Address = ?, House_Nr = ?, Postal_Code = ?, Telephone = ?, City = ? WHERE ID = ?'
  pool.query(
    sql,
    [item.Name, item.Address, item.House_Nr, item.Postal_Code, item.Telephone, item.City, homeID],
    (error, result) => {
      if (error) {
        callback(error, undefined)
      } else if (result.length === 0) {
        callback({ message: 'studenthome not found', errCode: 404 }, undefined)
      } else {
        callback(undefined, item)
      }
    }
  )
}

const updateMeal = (mealId, item, callback) => {
  const sql = 'UPDATE meal SET ? WHERE ID = ?'
  pool.query(sql, [item, mealId], (error, result) => {
    if (error) {
      callback(error, undefined)
    } else if (result.length === 0) {
      callback({ message: 'meal not found', errCode: 404 }, undefined)
    } else {
      callback(undefined, item)
    }
  })
}

const remove = (id, callback) => {
  pool.query('DELETE FROM studenthome WHERE ID = ?', id, (error, result) => {
    if (error) {
      callback(error, undefined)
    } else if (result.length === 0) {
      callback({ message: 'studenthome not found', errCode: 404 }, undefined)
    } else {
      callback(undefined, undefined)
    }
  })
}

const removeMeal = (id, callback) => {
  pool.query('DELETE FROM meal WHERE ID = ?', id, (error, result) => {
    if (error) {
      callback(error, undefined)
    } else if (result.length === 0) {
      callback({ message: 'meal not found', errCode: 404 }, undefined)
    } else {
      callback(undefined, undefined)
    }
  })
}

module.exports = {
  pool,
  add,
  addMeal,
  getAll,
  getAllMeals,
  getByCity,
  getByName,
  getByCityAndName,
  getById,
  getByIdMeal,
  update,
  updateMeal,
  remove,
  removeMeal
}
