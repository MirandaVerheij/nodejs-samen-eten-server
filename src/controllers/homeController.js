const database = require('../dao/database')
const assert = require('assert')
const logger = require('tracer').console()
const jwt_decode = require('jwt-decode')
const MISSING_STRING = 'is missing or not a string'
const MISSING_NUMBER = 'is missing or not a number'
const INVALID = 'is invalid'
const VALID = 'is valid'

module.exports = {
  validatePostalCode: (req, res, next) => {
    try {
      const dutchPostalCodeFormat = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i
      /*Regex: The postal code must begin with a digit, which is not 0 followed by 3 digits and 
      an optional space. After that there should be two letters from a-z, but the combinations SA, SD and SS
      are not allowed. The i makes this regex case-insensitive.*/
      assert(dutchPostalCodeFormat.test(req.body.Postal_Code), 'postal code ' + INVALID)
      logger.info('postal code ' + VALID)
      next()
    } catch (error) {
      logger.error('postal code is invalid')
      next({ message: 'postal code is invalid', errCode: 400 })
    }
  },

  validateTelephoneNumber: (req, res, next) => {
    try {
      const dutchPhoneFormat = /^0\d{9}$/
      //Regex: The telephone number must begin with a 0 followed by 9 digits.
      assert(dutchPhoneFormat.test(req.body.Telephone), 'telephone number ' + INVALID)
      logger.info('telephone number ' + VALID)
      next()
    } catch (error) {
      logger.error('telephone number is invalid')
      next({ message: 'telephone number is invalid', errCode: 400 })
    }
  },

  validateStudenthome(req, res, next) {
    logger.info('Studenthome validation started for: ' + JSON.stringify(req.body))
    try {
      const { Name, Address, House_Nr, UserID, Postal_Code, City, Telephone } = req.body
      assert(typeof Name === 'string', 'name ' + MISSING_STRING)
      assert(typeof Address === 'string', 'address ' + MISSING_STRING)
      assert(typeof House_Nr === 'number', 'house number ' + MISSING_NUMBER)
      assert(typeof UserID === 'number', 'user ID ' + MISSING_NUMBER)
      assert(typeof Postal_Code === 'string', 'postal code ' + MISSING_STRING)
      assert(typeof City === 'string', 'city ' + MISSING_STRING)
      assert(typeof Telephone === 'string', 'telephone number ' + MISSING_STRING)

      logger.info('studenthome ' + VALID)
      next()
    } catch (error) {
      logger.error('studenthome ' + INVALID)
      next({ message: error.message, errCode: 400 })
    }
  },

  validateStudenthomeNonExistent: (req, res, next) => {
    database.getById(req.params.homeId, (error, result) => {
      if (error) {
        logger.info(error.message)
        res.status(404).json({ message: 'studenthome does not exist', errcode: 400 })
      }
      if (result) {
        next()
      }
    })
  },

  create: (req, res, next) => {
    const studenthome = req.body

    database.add(studenthome, (error, result) => {
      if (error) {
        logger.error('Error adding studenthome', studenthome)
        res.status(400).json({ message: 'studenthome already exists' })
      }
      if (result) {
        logger.info(result)
        res.status(200).json({ status: 'success', result: result })
      }
    })
  },

  get: (req, res, next) => {
    logger.info('\nFiltering on: name = ' + req.query.name + ' and city = ' + req.query.city)
    if (req.query.name === undefined && req.query.city === undefined) {
      database.getAll((err, result) => {
        if (err) {
          next(err)
        }
        if (result) {
          logger.info(result)
          res.status(200).json({ status: 'success', result: result })
        }
      })
    } else if (req.query.name !== undefined && req.query.city === undefined) {
      database.getByName(req.query.name, (err, result) => {
        if (err) {
          next(err)
        }
        if (result) {
          logger.info(result)
          res.status(200).json({ status: 'success', result: result })
        }
      })
    } else if (req.query.name === undefined && req.query.city !== undefined) {
      database.getByCity(req.query.city, (err, result) => {
        if (err) {
          next(err)
        }
        if (result) {
          logger.info(result)
          res.status(200).json({ status: 'success', result: result })
        }
      })
    } else {
      database.getByCityAndName(req.query.city, req.query.name, (err, result) => {
        if (err) {
          next(err)
        }
        if (result) {
          logger.info(result)
          res.status(200).json({ status: 'success', result: result })
        }
      })
    }
  },

  getById: (req, res, next) => {
    logger.info('Filtering on: home ID: ' + req.params.homeId)
    let homeId = req.params.homeId

    database.getById(homeId, (error, result) => {
      if (error) {
        next(error)
      }
      if (result) {
        logger.info(result)
        res.status(200).json({ status: 'success', result: result })
      }
    })
  },

  update: (req, res, next) => {
    const studenthome = req.body
    const authHeader = req.headers.authorization
    const token = authHeader.substring(7, authHeader.length)
    const decoded = jwt_decode(token)
    const userID = decoded.id

    database.getById(req.params.homeId, (error, result) => {
      if (error) {
        next(error)
      }
      if (result) {
        const adminID = result[0].UserID
        logger.info('The user ID of the admin user is: ' + adminID)
        logger.info('The user ID of this user is: ' + userID)
        if (userID == adminID) {
          logger.info('The user ID and admin ID match')
          database.update(req.params.homeId, studenthome, (error, result) => {
            if (error) {
              next(error)
            }
            if (result) {
              logger.info(result)
              res.status(200).json({ status: 'success', result: result })
            }
          })
        } else {
          logger.info('The userID en admin ID do not match')
          next(
            { message: 'user does not have permission to change this studenthome', errCode: 401 },
            undefined
          )
        }
      }
    })
  },

  delete: (req, res, next) => {
    const authHeader = req.headers.authorization
    const token = authHeader.substring(7, authHeader.length)
    const decoded = jwt_decode(token)
    const userID = decoded.id

    database.getById(req.params.homeId, (error, result) => {
      if (error) {
        next(error)
      }
      if (result) {
        const adminID = result[0].UserID
        logger.info('The userID of the admin user is:' + adminID)
        logger.info('The userID of this user is:' + userID)
        if (userID == adminID) {
          logger.info('The user ID and admin ID match')
          database.remove(req.params.homeId, (error, result) => {
            if (error) {
              next(error)
            } else {
              logger.info('studenthome was deleted.')
              res.status(200).json({ status: 'success' })
            }
          })
        } else {
          logger.info('The userID en admin ID do not match')
          next(
            { message: 'user does not have permission to change this studenthome', errCode: 401 },
            undefined
          )
        }
      }
    })
  }
}
