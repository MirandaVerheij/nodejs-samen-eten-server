//
// Authentication controller
//
const assert = require('assert')
const jwt = require('jsonwebtoken')
const pool = require('../dao/database').pool
const logger = require('../dao/config').logger
const jwtSecretKey = require('../dao/config').jwtSecretKey

module.exports = {
  login(req, res, next) {
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error('Error getting connection from pool')
        res.status(500).json({ error: err.toString(), datetime: new Date().toISOString() })
      }
      if (connection) {
        // 1. Kijk of deze useraccount bestaat.
        connection.query(
          'SELECT `ID`, `Email`, `Password`, `First_Name`, `Last_Name` FROM `user` WHERE `Email` = ?',
          [req.body.email],
          (err, rows, fields) => {
            connection.release()
            if (err) {
              logger.error('Error: ', err.toString())
              res.status(400).json({ message: 'User not found', errcode: 400 })
            } else {
              // 2. Er was een resultaat, check het password.
              logger.info('Result from database: ')
              logger.info(rows)
              if (rows && rows.length === 1 && rows[0].Password == req.body.password) {
                logger.info('passwords DID match, sending valid token')
                // Create an object containing the data we want in the payload.
                const payload = {
                  id: rows[0].ID
                }
                // Userinfo returned to the caller.
                const userinfo = {
                  id: rows[0].ID,
                  firstName: rows[0].First_Name,
                  lastName: rows[0].Last_Name,
                  emailAdress: rows[0].Email,
                  token: jwt.sign(payload, jwtSecretKey, { expiresIn: '2h' })
                }
                logger.debug('Logged in, sending: ', userinfo)
                res.status(200).json(userinfo)
              } else {
                logger.info('User not found or password invalid')
                res.status(400).json({ message: 'User not found or password invalid', errcode: 400 })
              }
            }
          }
        )
      }
    })
  },

  validateLogin(req, res, next) {
    // Verify that we receive the expected input
    try {
      assert(typeof req.body.email === 'string', 'email address is missing or not a string')
      assert(typeof req.body.password === 'string', 'password is missing or not a string')
      next()
    } catch (error) {
      res.status(400).json({ message: error.message, errcode: 400 })
    }
  },

  register(req, res, next) {
    logger.info('register')
    logger.info(req.body)

    /**
     * Query the database to see if the email of the user to be registered already exists.
     */
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error('Error getting connection from pool: ' + err.toString())
        res.status(500).json({ error: ex.toString(), datetime: new Date().toISOString() })
      }
      if (connection) {
        let { firstname, lastname, email, studentnr, password } = req.body

        connection.query(
          'INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES (?, ?, ?, ?, ?)',
          [firstname, lastname, email, studentnr, password],
          (err, rows, fields) => {
            connection.release()
            if (err) {
              // When the INSERT fails, we assume the user already exists
              logger.error('Error: ' + err.toString())
              res.status(400).json({ message: 'This email address has already been taken.', errcode: 400 })
            } else {
              logger.trace(rows)
              const payload = {
                id: rows.insertId
              }
              const userinfo = {
                id: rows.insertId,
                firstName: firstname,
                lastName: lastname,
                emailAdress: email,
                token: jwt.sign(payload, jwtSecretKey, { expiresIn: '2h' })
              }
              logger.debug('Registered', userinfo)
              res.status(200).json(userinfo)
            }
          }
        )
      }
    })
  },

  validateRegister(req, res, next) {
    try {
      assert(typeof req.body.firstname === 'string', 'firstname is missing or not a string')
      assert(typeof req.body.lastname === 'string', 'lastname  is missing or not a string')
      assert(typeof req.body.email === 'string', 'email  is missing or not a string')
      assert(typeof req.body.password === 'string', 'password is missing or not a string')
      next()
    } catch (error) {
      logger.debug('validateRegister error: ', error.toString())
      next({ message: error.message, errCode: 400 })
    }
  },

  validateEmail(req, res, next) {
    try {
      const validator =
        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      assert(validator.test(req.body.email), 'email address is invalid')
      logger.info('email address is valid')
      next()
    } catch (error) {
      logger.error('email address is invalid')
      next({ message: error.message, errCode: 400 })
    }
  },

  validatePassword(req, res, next) {
    try {
      //Password must contain: digit, uppercase, lowercase, 8-32 characters total.
      const validator = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/
      assert(validator.test(req.body.password), 'password is invalid')
      logger.info('password is valid')
      next()
    } catch (error) {
      logger.error('password is invalid')
      next({ message: error.message, errCode: 400 })
    }
  },

  validateToken(req, res, next) {
    logger.info('validateToken called')
    const authHeader = req.headers.authorization
    if (!authHeader) {
      logger.warn('authorization header missing!')
      res.status(401).json({ message: 'authorization header missing!', errcode: 401 })
    } else {
      // Strip the word 'Bearer ' from the headervalue
      const token = authHeader.substring(7, authHeader.length)

      jwt.verify(token, jwtSecretKey, (err, payload) => {
        if (err) {
          logger.warn('Not authorized')
          res.status(401).json({ message: 'user is not the owner', errcode: 401 })
        }
        if (payload) {
          logger.debug('token is valid', payload)
          // User heeft toegang. Voeg UserId uit payload toe aan
          // request, voor ieder volgend endpoint.
          req.userId = payload.id
          next()
        }
      })
    }
  }
}
