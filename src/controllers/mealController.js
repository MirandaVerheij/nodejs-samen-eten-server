const database = require('../dao/database')
const assert = require('assert')
const logger = require('tracer').console()
const jwt_decode = require('jwt-decode')

module.exports = {
  validateMeal(req, res, next) {
    logger.info('Meal validation started for: ' + JSON.stringify(req.body))

    try {
      const {
        Name,
        Description,
        Ingredients,
        Allergies,
        CreatedOn,
        OfferedOn,
        Price,
        MaxParticipants,
        UserID
      } = req.body
      assert(typeof Name === 'string', 'name is missing or not a string')
      assert(typeof Description === 'string', 'description is missing or not a string')
      assert(typeof Ingredients === 'string', 'ingredients is missing or not a string')
      assert(typeof Allergies === 'string', 'allergies is missing or not a string')
      assert(typeof CreatedOn === 'string', 'createdOn is missing or not a string')
      assert(typeof OfferedOn === 'string', 'offeredOn is missing or not a string')
      assert(typeof Price === 'number', 'price is missing or not a number')
      assert(Price >= 0, 'the given value of price is negative')
      assert(typeof MaxParticipants === 'number', 'maxParticipants is missing or not a number')
      assert(typeof UserID === 'number', 'userID is missing or not a number')

      logger.info('meal is valid')
      next()
    } catch (error) {
      logger.error('meal is invalid')
      next({ message: error.message, errCode: 400 })
    }
  },

  validateMealNonExistent: (req, res, next) => {
    database.getByIdMeal(req.params.mealId, (error, result) => {
      if (error) {
        logger.info(error.message)
        res.status(404).json({ message: error.message, errcode: error.errCode })
      }
      if (result) {
        next()
      }
    })
  },

  create: (req, res, next) => {
    const meal = req.body

    database.addMeal(meal, (error, result) => {
      if (error) {
        logger.error('Error adding meal ', meal)
        res.status(400).json({ message: 'studenthome does not exist' })
      }
      if (result) {
        logger.info(result)
        res.status(200).json({ status: 'success', result: result })
      }
    })
  },

  get: (req, res, next) => {
    database.getAllMeals(req.params.homeId, (err, result) => {
      if (err) {
        next(err)
      }
      if (result) {
        logger.info(result)
        res.status(200).json({ status: 'success', result: result })
      }
    })
  },

  getById: (req, res, next) => {
    let mealId = req.params.mealId
    logger.info('Filtering on: meal ID: ' + mealId)

    database.getByIdMeal(mealId, (error, result) => {
      if (error) {
        next(error)
      }
      if (result) {
        logger.info(result)
        res.status(200).json({ status: 'success', result: result })
      }
    })
  },

  update: (req, res, next) => {
    const meal = req.body
    const mealId = req.params.mealId
    const authHeader = req.headers.authorization
    const token = authHeader.substring(7, authHeader.length)
    const decoded = jwt_decode(token)
    const userID = decoded.id

    database.getByIdMeal(mealId, (error, result) => {
      if (error) {
        next(error)
      }
      if (result) {
        const adminID = result[0].UserID
        logger.info('The user ID of the admin user is: ' + adminID)
        logger.info('The user ID of this user is: ' + userID)
        if (userID == adminID) {
          logger.info('The user ID and admin ID match')
          database.updateMeal(mealId, meal, (error, result) => {
            if (error) {
              next(error)
            }
            if (result) {
              logger.info(result)
              res.status(200).json({ status: 'success', result: result })
            }
          })
        } else {
          logger.info('The userID en admin ID do not match')
          next({ message: 'user does not have permission to change this meal', errCode: 401 }, undefined)
        }
      }
    })
  },

  delete: (req, res, next) => {
    const authHeader = req.headers.authorization
    const token = authHeader.substring(7, authHeader.length)
    const decoded = jwt_decode(token)
    const userID = decoded.id

    database.getByIdMeal(req.params.mealId, (error, result) => {
      if (error) {
        next(error)
      }
      if (result) {
        const adminID = result[0].UserID
        logger.info('The userID of the admin user is:' + adminID)
        logger.info('The userID of this user is:' + userID)
        if (userID == adminID) {
          logger.info('The user ID and admin ID match')
          database.removeMeal(req.params.mealId, (error, result) => {
            if (error) {
              next(error)
            } else {
              logger.info('meal was deleted.')
              res.status(200).json({ status: 'success' })
            }
          })
        } else {
          logger.info('The userID en admin ID do not match')
          next({ message: 'user does not have permission to change this meal', errCode: 401 }, undefined)
        }
      }
    })
  }
}
